﻿using System;
using System.Reflection;

namespace Task1
{
    public interface IContainer
    {
        void AddAssembly(Assembly assembly);
        void AddType(Type type);
        void AddType(Type type, Type baseType);
        T Get<T>() where T: class;
    }
}