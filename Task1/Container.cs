﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Task1.DoNotChange;

namespace Task1
{
    public class Container : IContainer
    {
        private readonly Type ExportAttributeType = typeof(ExportAttribute);
        private readonly Type ImportAttributeType = typeof(ImportAttribute);
        private readonly Type ImportConstructorAttributeType = typeof(ImportConstructorAttribute);
        private readonly Dictionary<Type, Type> _registeredTypesWithImplementations = new Dictionary<Type, Type>();

        public void AddAssembly(Assembly assembly)
        {
            var typesToRegister = assembly.GetTypes()
                .Where(t => 
                    t.GetCustomAttributes(ExportAttributeType).Any() ||
                    t.GetCustomAttributes(ImportConstructorAttributeType).Any() ||
                    t.GetProperties().Any(p => p.GetCustomAttributes(ImportAttributeType).Any()));

            foreach (var type in typesToRegister)
            {
                if (type.GetCustomAttributes(ExportAttributeType).Any())
                {
                    var exportAttribute = type.GetCustomAttributes().First(a => a.GetType() == ExportAttributeType) as ExportAttribute;

                    if (exportAttribute?.Contract != null)
                    {
                        AddType(type, exportAttribute.Contract);
                        continue;
                    }
                }

                AddType(type);
            }
        }

        public void AddType(Type type) => _registeredTypesWithImplementations.Add(type, type);

        public void AddType(Type type, Type baseType)
        {
            _registeredTypesWithImplementations.Add(baseType, type);
            _registeredTypesWithImplementations.Add(type, type);
        }

        public T Get<T>() where T: class => Get(typeof(T)) as T;

        private object Get(Type type)
        {
            var targetType = _registeredTypesWithImplementations[type] ??
                             throw new InvalidOperationException($"Type {type.FullName} is not registered.");

            return targetType.GetCustomAttributes(ImportConstructorAttributeType).Any() ? 
                GetImportConstructorInstance(targetType) : 
                GetImportPropertiesInstance(targetType);
        }

        private object GetImportConstructorInstance(Type targetType)
        {
            var parameters = targetType
                .GetConstructors()
                .FirstOrDefault()
                ?.GetParameters()
                .Select(p => Get(p.ParameterType))
                .ToArray();

            return targetType.GetConstructors().FirstOrDefault()?.Invoke(parameters);
        }

        private object GetImportPropertiesInstance(Type targetType)
        {
            var instance = Activator.CreateInstance(targetType);

            targetType
                .GetProperties()
                .Where(p => p.GetCustomAttributes(ImportAttributeType).Any())
                .ToList()
                .ForEach(p => p.SetValue(instance, Get(p.PropertyType)));

            return instance;
        }
    }
}