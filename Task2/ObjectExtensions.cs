﻿using System;
using System.Linq;
using System.Reflection;

namespace Task2
{
    public static class ObjectExtensions
    {
        public static void SetReadOnlyProperty(this object obj, string propertyName, object newValue)
        {
            SearchPropTypeHierarchyRecursive(obj.GetType(), obj, propertyName, newValue);
        }

        public static void SetReadOnlyField(this object obj, string filedName, object newValue)
        {
            var field = obj.GetType().GetField(filedName) ??
                        throw new ArgumentException();

            field.SetValue(obj, newValue);
        }

        /// <summary>
        /// Is used to recursively iterate through the class hierarchy and search for a private auto-property-baking field.
        /// If such baking field is not found though all the hierarchy, throws an ArgumentException 
        /// </summary>
        /// <param name="type">Type to search field in</param>
        /// <param name="obj">Target instance to replace property in</param>
        /// <param name="propertyName">Name of read-only property</param>
        /// <param name="newValue">Value used to replace old property value</param>
        private static void SearchPropTypeHierarchyRecursive(Type type, object obj, string propertyName, object newValue)
        {
            if (type == typeof(object))
            {
                throw new ArgumentException($"No property {propertyName} found.");
            }

            var prop = type.GetField($"<{propertyName}>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic);

            if (prop == null)
            {
                SearchPropTypeHierarchyRecursive(obj.GetType().BaseType, obj, propertyName, newValue);
                return;
            }

            prop.SetValue(obj, newValue);
        }
    }
}
